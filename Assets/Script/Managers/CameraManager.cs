using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : ManagerBase, IService
{
    [SerializeField]
    private Camera mainCamera;
    [SerializeField]
    private Camera gameCamera;

    public Camera MainCamera { get => mainCamera; set => mainCamera = value; }
    public Camera GameCamera { get => gameCamera; set => gameCamera = value; }
    void Awake()
    {
        Register();
    }
    public void Register()
    {
        ServiceLocatorManager.ReplaceService(this);
    }
    public void ZoomEvent(float duration, Vector3 targetPos,float size,Action action)
    {
        StartCoroutine(UpdateNewCameraSize(duration, targetPos, size, action));
    }
    IEnumerator UpdateNewCameraSize(float duration,Vector3 targetPos,float size, Action action)
    {
        float timeElapsed = 0;
        Vector3 nowPos = mainCamera.transform.position;
        float defaultSize = mainCamera.orthographicSize;
        Vector3 defaultPso = mainCamera.transform.position;
        targetPos = new Vector3(targetPos.x, targetPos.y, -10);
        while (true)
        {
            if (timeElapsed >= duration)
            {
                action?.Invoke();
                mainCamera.orthographicSize = defaultSize;
                mainCamera.transform.position = defaultPso;
                break;
            }
            mainCamera.orthographicSize = Mathf.Lerp(defaultSize, size, timeElapsed / duration);
            mainCamera.transform.position = Vector3.Lerp(nowPos, targetPos, timeElapsed / duration);
            timeElapsed += Time.deltaTime;
            yield return null;
        }
    }

    public override void ResetAllEvent()
    {
       
    }
}
