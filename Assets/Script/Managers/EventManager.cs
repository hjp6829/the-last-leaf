using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : ManagerBase, IService
{
    public EventConnecter EventConnecter { get => eventConnecter; set => eventConnecter = value; }
    private EventConnecter eventConnecter;
    void Awake()
    {
        Register();
    }
    public void Register()
    {
        ServiceLocatorManager.Register(this);
    }
    public EventContainer GetEvent(EventType type)
    {
        return eventConnecter.GetEvent(type);
    }
    public void ActiveAction(EventType type)
    {
        eventConnecter.ActiveEvent(type);
    }

    public override void ResetAllEvent()
    {
       
    }
}
