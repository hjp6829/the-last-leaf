using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ManagerBase : SerializedMonoBehaviour
{
    public abstract void ResetAllEvent();
}
