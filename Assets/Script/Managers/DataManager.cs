using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.IO;
using System;
using static UnityEngine.Mesh;
#region GoogleSheetDataClass
[Serializable]
public class GoogleSheetData
{
    [SerializeField]
    int idx;
    [SerializeField]
    List<string> tableNames;
    [SerializeField]
    List<string> columns;
    public GoogleSheetData(int tableidx)
    {
        idx=tableidx;
        tableNames=new List<string>();
        columns=new List<string>();
    }
    public void SetTableData(string tablename,string column)
    {
        tableNames.Add(tablename);
        columns.Add(column);
    }
   public Dictionary<string, string> GetData()
    {
        Dictionary<string, string> temp=new Dictionary<string, string>();
        for (int i=0;i<tableNames.Count;i++)
        {
            temp.Add(tableNames[i], columns[i]);
        }
        return temp;
    }
    public int GetIDX()
    {
        return idx;
    }
}
[Serializable]
public class GoogleSheetDatas
{
    [SerializeField]
    List<GoogleSheetData> list=new List<GoogleSheetData>();
    [SerializeField]
    private string sheetName;
    public void SetSheetName(string sheetName)
    {
        this.sheetName=sheetName;
    }
    public string GetSheetName()
    {
        return sheetName;
    }
    public void Setdata(List<GoogleSheetData> data)
    {
        list = data;
    }
    public List<GoogleSheetData> Getdata()
    {
        return list;
    }
}
[Serializable]
public class GoogleSheetsWrapper
{
    [SerializeField]
    public List<GoogleSheetDatas> dataList = new List<GoogleSheetDatas>();
    public GoogleSheetsWrapper()
    {
        dataList = new List<GoogleSheetDatas>();
    }
    public void SetData(string sheetname, List<GoogleSheetData> data)
    {
        GoogleSheetDatas temp = new GoogleSheetDatas();
        temp.SetSheetName(sheetname);
        temp.Setdata(data);
        dataList.Add(temp);
    }
    public Dictionary<string, Dictionary<int, Dictionary<string, string>>> GetTable()
    {
        Dictionary<string, Dictionary<int, Dictionary<string, string>>> finalData = new Dictionary<string, Dictionary<int, Dictionary<string, string>>>();
        foreach(var data in dataList)
        {
            Dictionary<int, Dictionary<string, string>> temp = new Dictionary<int, Dictionary<string, string>>();
            foreach (GoogleSheetData sheetdata in data.Getdata())
            {
                temp.Add(sheetdata.GetIDX(), sheetdata.GetData());
            }
            finalData.Add(data.GetSheetName(), temp);
        }

        return finalData;
    }
}
#endregion
[Serializable]
public class DialogueData
{
    [SerializeField]
    public int StartIDX;
    [SerializeField]
    public int EndIDX;
    [SerializeField]
    public int NextDialogueDataIDX;
}
public class GameData
{
    public float BGMValue;
    public float EffectValue;
    public bool isGuest;
    public bool isFirst=true;
}
public class DataManager : ManagerBase, IService
{
    public GameData SaveDataFile { get => dataFile; }

    [SerializeField]
    private Dictionary<int,DialogueData> dialogueDatas = new Dictionary<int,DialogueData>();
    string path = Application.streamingAssetsPath + "/GoogleSheetData.json";
    private Dictionary<string, Dictionary<int, Dictionary<string, string>>>  tableElemental;
    private List<string> sheetNameListnew = new List<string> { "normal", "bad", "happy" };
    private List<int> sheetIdListnew = new List<int> { 1758866726, 980063653, 1836295957 };
    private GameData dataFile;
    private void Awake()
    {
        Register();
        GoogleSheetsWrapper temp = LoadJsonFile<GoogleSheetsWrapper>("GoogleSheetData.json");
        tableElemental = temp.GetTable();
        dataFile = LoadSaveJsonFile<GameData>("SaveData.json");
        //ReadSheetData();
    }
    public void SaveDataFileJson()
    {
#if UNITY_EDITOR
        File.WriteAllText(Application.dataPath + "/SaveData.json", JsonUtility.ToJson(dataFile, false));
#elif UNITY_ANDROID
        string path = Application.persistentDataPath + "/SaveData.json";
        File.WriteAllText(path, JsonUtility.ToJson(dataFile, false));
          Debug.Log("안드로이드 저장");
#endif
    }
    private void NewSaveDataJson(string fileName)
    {
        GameData newSaveData= new GameData();
        newSaveData.BGMValue = 1;
        newSaveData.EffectValue = 1;
        newSaveData.isFirst = true;
        newSaveData.isGuest = false;
#if UNITY_EDITOR
        string jsonData = JsonUtility.ToJson(newSaveData, true);
        string path = Path.Combine(Application.dataPath, fileName);
        File.WriteAllText(path, jsonData);
#elif UNITY_ANDROID
        string filePath = Application.persistentDataPath + "/"+fileName;
        File.WriteAllText(filePath, JsonUtility.ToJson(newSaveData, false));
#endif
    }
    #region GoogleSheetRead
    [Button(Name = "ReadNewSheetData")]
    private void ReadSheetData()
    {
        StartCoroutine(loadSheetData());
    }
    IEnumerator loadSheetData()
    {
        GoogleSheetsWrapper sheetsWrapper = new GoogleSheetsWrapper();
        for (int x=0;x< sheetIdListnew.Count;x++)
        {
            string sheetAdress = "https://docs.google.com/spreadsheets/d/15_zxJjO8j9dWAW4nfU0OHyNRV93mzRR3uc2YpsE4izo/export?format=tsv&range=A1:C&gid="+ sheetIdListnew[x].ToString();
            using (UnityWebRequest www = UnityWebRequest.Get(sheetAdress))
            {
                yield return www.SendWebRequest();
                string temp = www.downloadHandler.text;
                string[] rows = temp.Split('\n');
                string[] valueKeys = rows[0].Split('\t');

                List<GoogleSheetData> datalist= new List<GoogleSheetData>();
                for (int i = 1; i < rows.Length; i++)
                {
                    string[] colums = rows[i].Split('\t');
                    int idx = int.Parse(colums[0]);
                    GoogleSheetData tempDataSheet = new GoogleSheetData(idx);
                    for (int j = 1; j < colums.Length; j++)
                    {
                        string valueKeyTemp = valueKeys[j].Replace("\r", "");
                        string columsTemp = colums[j].Replace("\r", "");
                        tempDataSheet.SetTableData(valueKeyTemp, columsTemp);
                    }
                    datalist.Add(tempDataSheet);
                }
                sheetsWrapper.SetData(sheetNameListnew[x], datalist);
                Debug.LogWarning("GoogleSheet " + sheetNameListnew[x] + "에서 정보를 읽어왔습니다");
            }
        }
        File.WriteAllText(path, JsonUtility.ToJson(sheetsWrapper, false));
        Debug.LogWarning("GoogleSheet에서 모든 정보를 읽어와 json으로 저장했습니다");
    }
    #endregion
    private T LoadJsonFile<T>(string fileName)               
    {
        string filePath;
        string jsondata;
#if UNITY_EDITOR
        filePath = Path.Combine(Application.streamingAssetsPath, fileName);
        jsondata = File.ReadAllText(filePath);
#elif UNITY_ANDROID
        filePath = Application.streamingAssetsPath + "/" + fileName;
        UnityWebRequest www = UnityWebRequest.Get(filePath);
        www.SendWebRequest();
        while (!www.isDone) { }
        jsondata = www.downloadHandler.text;
#endif
        return JsonUtility.FromJson<T>(jsondata);
    }
    private T LoadSaveJsonFile<T>(string fileName)
    {
        string filePath;
        string jsondata;

#if UNITY_EDITOR
        filePath = Path.Combine(Application.dataPath, fileName);
        if (!File.Exists(filePath)) // 파일이 존재하지 않는 경우
        {
            NewSaveDataJson(fileName);
            return LoadSaveJsonFile<T>(fileName);
        }
        jsondata = File.ReadAllText(filePath);

#elif UNITY_ANDROID
        filePath = Application.persistentDataPath + "/"+fileName;

        if (!File.Exists(filePath)) // 파일이 존재하지 않는 경우
        {  Debug.Log("파일없음");
            NewSaveDataJson(fileName);
            return LoadSaveJsonFile<T>(fileName);
        }
        jsondata = File.ReadAllText(filePath);
#endif
        return JsonUtility.FromJson<T>(jsondata);
    }
    public void Register()
    {
        ServiceLocatorManager.Register(this);
    }
    public Dictionary<string, string> GetDialogueByIDX(int idx)
    {
        return tableElemental["normal"][idx];
    }
    public Dictionary<string, string> GetEndByIDX(EndType endType,int idx)
    {
        if(endType== EndType.HAPPYENDING)
            return tableElemental["happy"][idx];
        return tableElemental["bad"][idx];

    }
    public DialogueData GetDialogueDataByIDX(int idx)
    {
        return dialogueDatas[idx];
    }
    public DialogueData GetGameEndDialogueDataByIDX(EndType endType)
    {
        DialogueData datatemp= new DialogueData();
        datatemp.StartIDX = 0;
        if (endType == EndType.HAPPYENDING)
            datatemp.EndIDX = tableElemental["happy"].Count-1;
        else
            datatemp.EndIDX = tableElemental["bad"].Count-1;
        return datatemp;
    }

    public override void ResetAllEvent()
    {
       
    }
}
