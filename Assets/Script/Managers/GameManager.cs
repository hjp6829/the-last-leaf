﻿//using GooglePlayGames;
using Sirenix.OdinInspector;
using System;
using UnityEngine;
//using GooglePlayGames.BasicApi;
//using GoogleMobileAds.Api;
public class GameManager : ManagerBase, IService
{
    [HideInInspector]
    public Action<int> OnDialogueCountStart;
    [HideInInspector]
    public Action<int> OnDialogueCountEnd;
    [HideInInspector]
    public Action<int> OnChecngeHP;
    [HideInInspector]
    public Action<bool> OnGamePause;

    //private InterstitialAd interstitialAd;

    public int DialogueCount { get => dialogueCount; set 
        {
            OnDialogueCountEnd?.Invoke(dialogueCount);
            dialogueCount = value;
            OnDialogueCountStart?.Invoke(dialogueCount);
        }
    }
    public int LeafHP { get => leafHP; set 
        {
            leafHP = value;
            LeafUpdateByHP(leafHP);
        } 
    }
    public int DialogueDataCount { get => dialogueDataCount; set => dialogueDataCount = value; }
    public EndType EndType { get => endtype; set => endtype = value; }

    private int dialogueCount = 0;
    private int dialogueDataCount = 0;
    private int leafHP=6;
    private EndType endtype;

    void Awake()
    {
        //dialogueDataCount = 3;
        Register();
        DontDestroyOnLoad(this);
       // PlayGamesPlatform.DebugLogEnabled = true;
       // PlayGamesPlatform.Activate();
        //MobileAds.Initialize((InitializationStatus initStatus) =>
       // {
       //     
       // });
    }
    private void Start()
    {
        DataManager datamanager = ServiceLocatorManager.GetService<DataManager>();
        if(datamanager.SaveDataFile.isFirst)
        {
            UIManager uimanager = ServiceLocatorManager.GetService<UIManager>();
            uimanager.GetUI<LoginUI>(UIType.LOGINUI).Open();
        }
    }

    public void HPUpdate()
    {
        LeafUpdateByHP(leafHP);
    }
    public override void ResetAllEvent()
    {
        OnDialogueCountStart = null;
        OnDialogueCountEnd = null;
        OnChecngeHP = null;
        OnGamePause = null;
        dialogueCount = 0;
        dialogueDataCount = 0;
        leafHP = 6;
    }
    public void Register()
    {
        ServiceLocatorManager.Register(this);
    }
    private void LeafUpdateByHP(int hp)
    {
        OnChecngeHP?.Invoke(hp);
    }

}
