using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : ManagerBase, IService
{
    [SerializeField]
    private AudioSource bgmSource;
    [SerializeField]
    private AudioSource effectSource;
    [SerializeField]
    private List<AudioClip> effectList = new List<AudioClip>();
    public float BGMVolume { get => bgmSource.volume; set => bgmSource.volume = value; }
    public float EffectVolume { get => effectSource.volume; set => effectSource.volume = value; }
    private void Awake()
    {
        Register();
    }
    private void Start()
    {
        // bgmSource.Play();
        DataManager datamanager = ServiceLocatorManager.GetService<DataManager>();
        bgmSource.volume = datamanager.SaveDataFile.BGMValue;
        effectSource.volume = datamanager.SaveDataFile.EffectValue;
        bgmSource.Play();
    }
    public void PlayEffect(int idx)
    {
        effectSource.PlayOneShot(effectList[idx]);
    }
    public void SetBGM(AudioClip clip)
    {
        bgmSource.clip= clip;
        bgmSource.Play();
    }
    public void SetEffect(AudioClip clip)
    {
        effectSource.PlayOneShot(clip);
    }

    public void Register()
    {
        ServiceLocatorManager.Register(this);
    }

    public override void ResetAllEvent()
    {
        
    }
}
