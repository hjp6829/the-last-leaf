using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
public class Loadingmanager : MonoBehaviour
{
    public static string NextSceneName;
    private void Start()
    {
        //StartCoroutine(LoadScene(loadAction));
        ServiceLocatorManager.AutoDeleteManager();
        SceneManager.LoadScene(NextSceneName);
    }
    public static void LoadScene(string name, Action action = null)
    {
        Time.timeScale = 1;
        NextSceneName = name;
        SceneManager.LoadScene("LoadingScene");
    }
    //IEnumerator LoadScene(Action action = null, float delay = 0)
    //{
    //    AsyncOperation op = SceneManager.LoadSceneAsync(NextSceneName);
    //    op.completed += activeAction;
    //    op.allowSceneActivation = false;
    //    float timer = 0.0f;
    //    while (!op.isDone)
    //    {
    //        yield return null;
    //        timer += 0.1f * Time.deltaTime;
    //        delay -= Time.deltaTime;
    //        if (delay <= 0)
    //        {
    //            op.allowSceneActivation = true;
    //            yield break;
    //        }
    //    }
    //}
}
