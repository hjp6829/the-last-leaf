using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : ManagerBase, IService
{
    [HideInInspector]
    public Action<bool> OnFadeStart;
    public UIConnecter UIConnector { get => uiconnecter; set => uiconnecter = value; }

    private UIConnecter uiconnecter;

    void Awake()
    {
        Register();
    }

    public T GetUI<T>(UIType type) where T : UIBase
    {
        return (T)uiconnecter.GetUI(type);
    }
    public void Register()
    {
        ServiceLocatorManager.Register(this);
    }

    public override void ResetAllEvent()
    {
        OnFadeStart = null;
    }
}
