using Spine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Spine.Unity;
using System;
using UnityEngine.Events;

public class SpineComponent : SerializedMonoBehaviour, ISpineCallback
{
    [SerializeField]
    [BoxGroup("Renderer")]
    private SkeletonAnimation _SkeletonAnimation;
    private SpineCoordinator spineCoordinator;
    private string currentName;
    private UnityEvent currentAction;
    private bool isAction;
    public SpineCoordinator SpineCoordinator
    {
        get
        {
            if (spineCoordinator != null)
            {
                return spineCoordinator;
            }
            if (_SkeletonAnimation == null)
            {
                Debug.LogError($"{ToString()}은 스파인이 아닙니다.");
            }
            spineCoordinator = new SpineCoordinator(this, _SkeletonAnimation);
            spineCoordinator.Initialize();
            if (spineCoordinator.CanLoad())
            {
                spineCoordinator.Load();
            }

            return spineCoordinator;
        }
    }
    protected virtual void Start()
    {
        
    }
    public void AddSpine(string name, bool value)
    {
        SpineCoordinator.Add(name, value);
    }
    public virtual void OnSpineComplete(TrackEntry trackEntry)
    {
        if (!isAction)
            return;
        isAction = false;
        if(trackEntry.Animation.Name==currentName)
        {
            currentAction?.Invoke();
            currentAction = null;
        }
    }

    public virtual void OnSpineEnd(TrackEntry trackEntry)
    {

    }

    public virtual void OnSpineEvent(TrackEntry trackEntry, string eventName)
    {
       switch(eventName)
        {
            case "test":
                AudioManager audiomanager = ServiceLocatorManager.GetService<AudioManager>();
                audiomanager.PlayEffect(0);
                break;
        }
    }

    public virtual void OnSpineStart(TrackEntry trackEntry)
    {
       
    }

    public void SetSkin(string name)
    {
        SpineCoordinator.SetSkin(name);
        //SetSpineAnim("None", false);
    }

    public void SetSpine(SkeletonDataAsset asset)
    {
        //_SkeletonAnimation.skeletonDataAsset = asset;
        //var skinName = _SkeletonAnimation.initialSkinName;
        //SpineAsset newspine = new SpineAsset(_SkeletonAnimation.skeletonDataAsset, _SkeletonAnimation.initialSkinName == "" ? "default" : skinName, _SkeletonAnimation.AnimationName);
        //SpineCoordinator.LoadSpine = newspine;
        //SpineCoordinator.Initialize();
        //SpineCoordinator.Load();
    }

    public bool SetSpineAnim(string name, bool value)
    {
        SpineCoordinator.Set(name, value);
        return true;
    }
    public bool SetSpineAnim(string name, bool value, UnityEvent action)
    {
        isAction = true;
        currentName = name;
        currentAction = action;
        SpineCoordinator.Set(name, value);
        return true;
    }
    public void SetSpineTimeScale(float value)
    {
        throw new System.NotImplementedException();
    }
}

