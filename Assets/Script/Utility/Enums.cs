public enum UIType
{
    ETEXTBLOCK,
    MINIGAME0,
    TESTCLICK,
    MINIGAME1,
    OPTION,
    LOGINUI,
    GAMEOPTION,
    THEEND,
}
public enum EventType
{
    MINIGAME0START,
    TEXTBOXSETDATA,
    TEXTBOXSTART,
    TEXTBOXSTOP,
    MINIGAME1START,
    FLOWERSPINE,
    GAMEFAIL,
    GAMESUCCESS,
}
public enum EndType
{
    NULL,
    HAPPYENDING,
    BADENDING,
}
public enum GameEndType
{
    GAMESUCCESS,
    GAMEFAIL,
}
