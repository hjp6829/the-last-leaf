using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
[Serializable]
public class TestEvent
{
    public bool isStartEvent;
    public UnityEvent Event;
}
public class EventConnecter : SerializedMonoBehaviour
{
    #region makeEvent
    [TabGroup("MakeEvent")]
    [SerializeField]
    private EventType eventType;
    [TabGroup("MakeEvent")]
    [SerializeField]
    private Dictionary<EventType, EventContainer> eventsDic=new Dictionary<EventType, EventContainer>();
    #endregion
    [TabGroup("IDXEvent")]
    [SerializeField]
    private Dictionary<int, TestEvent>NormalEventDic=new Dictionary<int, TestEvent>();
    [TabGroup("IDXEvent")]
    [SerializeField]
    private Dictionary<int, TestEvent> HappyEndEventDic = new Dictionary<int, TestEvent>();
    [TabGroup("IDXEvent")]
    [SerializeField]
    private Dictionary<int, TestEvent> BadEndEventDic = new Dictionary<int, TestEvent>();
    private Dictionary<int, TestEvent> currentDic;
    private void Awake()
    {
        EventManager eventManager = ServiceLocatorManager.GetService<EventManager>();
        eventManager.EventConnecter = this;
        eventManager.GetEvent(EventType.GAMEFAIL).action += () =>
        {
            currentDic = BadEndEventDic;
        };
        eventManager.GetEvent(EventType.GAMESUCCESS).action += () =>
        {
            currentDic = HappyEndEventDic;
        };
    }
    private void Start()
    {
        var eventRegisters = FindObjectsOfType<SerializedMonoBehaviour>().OfType<IEventRegister>();
        foreach(var register in eventRegisters)
        {
            register.RegisterEvent(this);
        }
        currentDic = NormalEventDic;
        GameManager gamemanager = ServiceLocatorManager.GetService<GameManager>();
        gamemanager.OnDialogueCountEnd += (int value) =>
        {

            if (currentDic.ContainsKey(value) && !currentDic[value].isStartEvent)
            {
                Debug.Log("end이벤트 작동 : " + value);
                currentDic[value].Event?.Invoke();
            }
        };
        gamemanager.OnDialogueCountStart += (int value) =>
        {
            if (currentDic.ContainsKey(value) && currentDic[value].isStartEvent)
            {
                Debug.Log("start이벤트 작동 : " + value);
                currentDic[value].Event?.Invoke();
            }
        };
        EventManager eventManager = ServiceLocatorManager.GetService<EventManager>();
        eventManager.ActiveAction(EventType.TEXTBOXSETDATA);
    }
    IEnumerator coolTime(float time, Action action)
    {
        while (true)
        {
            if (time <= 0)
            {
                action?.Invoke();
                break;
            }
            time -= Time.deltaTime;
            yield return null;
        }
    }
    #region makeEvent
    [TabGroup("MakeEvent")]
    [Button("MakeNewEvent")]
    private void MakeNewEvent()
    {
        if(!eventsDic.ContainsKey(eventType))
        {
            GameObject temp = new GameObject();
            temp.AddComponent<EventContainer>();
            temp.name = eventType.ToString();
            temp.transform.SetParent(transform, false);
            eventsDic.Add(eventType, temp.GetComponent<EventContainer>());
        }
    }
    [TabGroup("MakeEvent")]
    [Button("DestroyEvent")]
    private void DestroyEvent()
    {
        if(eventsDic.ContainsKey(eventType))
        {
            DestroyImmediate(eventsDic[eventType].gameObject);
            eventsDic.Remove(eventType);
        }
    }
    public EventContainer GetEvent(EventType type)
    {
        if(eventsDic.ContainsKey(type))
        {
            return eventsDic[type];
        }
        else
        {
            Debug.LogError("이벤트컨테이너가 없습니다");
            return null;
        }
    }
    public void ActiveEvent(EventType type)
    {
        if(eventsDic.ContainsKey(type))
        {
            eventsDic[type].action?.Invoke();
        }
        else
        {
            Debug.LogError("이벤트컨테이너가 없습니다");
        }
    }
    #endregion
}
