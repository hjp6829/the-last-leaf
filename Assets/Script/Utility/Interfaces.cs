using Spine.Unity;

public interface IService
{
    void Register();
}
public interface IEventRegister
{
    void RegisterEvent(EventConnecter connecter);
}
public interface ISpineCallback
{
    void OnSpineStart(Spine.TrackEntry trackEntry);
    void OnSpineComplete(Spine.TrackEntry trackEntry);
    void OnSpineEnd(Spine.TrackEntry trackEntry);
    void OnSpineEvent(Spine.TrackEntry trackEntry, string eventName);
    void SetSpine(SkeletonDataAsset asset);
    bool SetSpineAnim(string name, bool value);
    void SetSpineTimeScale(float value);
    void SetSkin(string name);
    void AddSpine(string name, bool value);
}