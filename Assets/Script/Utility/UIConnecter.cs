using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIConnecter : SerializedMonoBehaviour
{
    [SerializeField] private List<UIBase> uis = new List<UIBase>();

    private void Awake()
    {
        UIManager uimanager = ServiceLocatorManager.GetService<UIManager>();
        uimanager.UIConnector = this;
        foreach (var ui in uis)
        {
            ui.initialze();
            if(ui.IsOpenStart)
                ui.Open();
        }
    }
    public UIBase GetUI(UIType uitype)
    {
        return uis.Find(x => x.UIType == uitype);
    }
}
