using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOptionButton : MonoBehaviour
{
    private bool isOptionOpen;
    private void Start()
    {
        UIManager uimanager = ServiceLocatorManager.GetService<UIManager>();
        uimanager.OnFadeStart += (bool value) =>
        {
            ToggleButton();
        };
    }
    public void ToggleOptionUI()
    {
        UIManager uimanager = ServiceLocatorManager.GetService<UIManager>();
        GameOption gameOption = uimanager.GetUI<GameOption>(UIType.GAMEOPTION);
        if (!isOptionOpen)
        {
            gameOption.Open();
            isOptionOpen = true;
            return;
        }
        gameOption.Close();
        isOptionOpen = false;
    }
    public void ToggleButton()
    {
        if(gameObject.activeSelf)
            gameObject.SetActive(false);
        else
            gameObject.SetActive(true);
    }
}
