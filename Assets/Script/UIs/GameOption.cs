using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOption : UIBase
{
    [SerializeField]
    private Slider BGMSlider;
    [SerializeField]
    private Slider EffectSlider;
    private AudioManager audiomanager;
    public void ToggleUI()
    {
        if (gameObject.activeSelf)
            Close();
        else
            Open();
    }
    public override void Open()
    {
        base.Open();
        GameManager gamemanager = ServiceLocatorManager.GetService<GameManager>();
        gamemanager.OnGamePause?.Invoke(true);
        audiomanager = ServiceLocatorManager.GetService<AudioManager>();
        BGMSlider.value = audiomanager.BGMVolume;
        EffectSlider.value = audiomanager.EffectVolume;
    }
    public void UpdateBGMValue()
    {
        audiomanager.BGMVolume = BGMSlider.value;
    }
    public void UpdateEffectValue()
    {
        audiomanager.EffectVolume = EffectSlider.value;
    }
    public override void Close()
    {
        base.Close();
        GameManager gamemanager = ServiceLocatorManager.GetService<GameManager>();
        gamemanager.OnGamePause?.Invoke(false);
        DataManager datamanager = ServiceLocatorManager.GetService<DataManager>();
        datamanager.SaveDataFile.EffectValue = EffectSlider.value;
        datamanager.SaveDataFile.BGMValue = BGMSlider.value;
        datamanager.SaveDataFileJson();
    }
    public void BackToMenuScene()
    {
        DataManager datamanager = ServiceLocatorManager.GetService<DataManager>();
        datamanager.SaveDataFile.EffectValue = EffectSlider.value;
        datamanager.SaveDataFile.BGMValue = BGMSlider.value;
        datamanager.SaveDataFileJson();
        Loadingmanager.LoadScene("Menu");
    }
}
