using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoginUI : UIBase
{
    //[SerializeField]
    //private AdmobBanner admobBanner;
    private string _status;
    public override void Open()
    {
        base.Open();
       // admobBanner.HideBanner();
    }
    public override void Close()
    {
        base.Close();
       // admobBanner.ShowBanner();
    }
    public void OnGoogleLogin()
    {
        if (!Social.localUser.authenticated)
        {
            Social.localUser.Authenticate((bool isOk, string error) =>
            {
                if (isOk)
                {
                    _status = Social.localUser.userName;
                    DataManager datamanager = ServiceLocatorManager.GetService<DataManager>();
                    datamanager.SaveDataFile.isFirst = false;
                    datamanager.SaveDataFile.isGuest = false;
                    datamanager.SaveDataFileJson();
                    Close();
                }
                else
                    _status = error;
            });
        }
    }
    public void OnGuestLogin()
    {
        DataManager datamanager = ServiceLocatorManager.GetService<DataManager>();
        datamanager.SaveDataFile.isFirst = false;
        datamanager.SaveDataFile.isGuest = true;
        datamanager.SaveDataFileJson();
        Close();
    }
}
