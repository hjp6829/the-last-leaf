using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public void OnGameStart()
    {
        Loadingmanager.LoadScene("GameScene");
    }
    public void OnOpenOptionUI()
    {
        UIManager uimanager = ServiceLocatorManager.GetService<UIManager>();
        uimanager.GetUI<Option>(UIType.OPTION).Open();
    }
}
