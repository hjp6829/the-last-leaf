using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Option : UIBase
{
    [SerializeField] private List<Sprite> volumeIconList;
    [SerializeField]
    private Slider BGMSlider;
    [SerializeField]
    private Slider EffectSlider;
    [SerializeField] private Image BGMIconImage;
    [SerializeField] private Image EffectIconImage;

    private string _status;
    private AudioManager audiomanager;
    public override void Open()
    {
        base.Open();
        audiomanager = ServiceLocatorManager.GetService<AudioManager>();
        BGMSlider.value = audiomanager.BGMVolume;
        EffectSlider.value = audiomanager.EffectVolume;
        SetVolumeIconImage(EffectIconImage, audiomanager.EffectVolume);
        SetVolumeIconImage(BGMIconImage, audiomanager.BGMVolume);
    }
    public void UpdateBGMValue()
    {
        audiomanager.BGMVolume = BGMSlider.value;
        SetVolumeIconImage(BGMIconImage, audiomanager.BGMVolume);
    }
    private void SetVolumeIconImage(Image image,float value)
    {
        value = value * 100;
        if(value==0)
        {
            image.sprite = volumeIconList[0];
        }
        else if(value>0&&value<=35)
            image.sprite = volumeIconList[1];
        else if(value>35&&value<=70)
            image.sprite = volumeIconList[2];
        else if(value>70&&value<=100)
            image.sprite = volumeIconList[3];
            
    }
    public void UpdateEffectValue()
    {
        audiomanager.EffectVolume = EffectSlider.value;
        SetVolumeIconImage(EffectIconImage, audiomanager.EffectVolume);
    }
    public override void Close()
    {
        base.Close();
        DataManager datamanager = ServiceLocatorManager.GetService<DataManager>();
        datamanager.SaveDataFile.EffectValue = EffectSlider.value;
        datamanager.SaveDataFile.BGMValue = BGMSlider.value;
        datamanager.SaveDataFileJson();
    }
    public void OnGoogleLogin()
    {
        if (!Social.localUser.authenticated)
        {
            Social.localUser.Authenticate((bool isOk, string error) =>
            {
                if (isOk)
                {
                    _status = Social.localUser.userName;
                    DataManager datamanager = ServiceLocatorManager.GetService<DataManager>();
                    datamanager.SaveDataFile.isGuest = false;
                    datamanager.SaveDataFileJson();
                    Close();
                }
                else
                    _status = error;
            });
        }
    }
}
