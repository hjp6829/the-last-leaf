using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using TMPro;
using UnityEngine;

public class CharacterTextBox : UIBase, IEventRegister
{
    [SerializeField]
    private TextMeshProUGUI characterNameTextBox;
    [SerializeField]
    private TextMeshProUGUI characterDialogueTextBox;
    [SerializeField]
    private float typeOutDelayTime = 0.3f;
    [SerializeField]
    private float nextDialogueDelay = 2.0f;
    private Coroutine dialogueCoroutine;
    private Coroutine autoPageChangeCoroutine;
    private string dialogueTextData;
    private bool isCoroutineActive = false;
    private DataManager dataManager;
    private GameManager gameManager;

    private int startIDX;
    private int currentIDX { get => gameManager.DialogueCount; set => gameManager.DialogueCount = value; }
    private int endIDX;
    private int nextDialougeIDX;
    private bool isEvent;
    private bool isGameEnd;
    private EndType endEventType;
    private bool isGamePause;
    public override void initialze()
    {
        base.initialze();
        dataManager = ServiceLocatorManager.GetService<DataManager>();
        gameManager = ServiceLocatorManager.GetService<GameManager>();
        gameManager.OnGamePause += (bool value) =>
        {
            isGamePause = value;
        };
        EventManager eventManager= ServiceLocatorManager.GetService<EventManager>();
        eventManager.GetEvent(EventType.TEXTBOXSTOP).action += () =>
        {
            isEvent = true;
            characterDialogueTextBox.text = "";
            Close();
        };
        eventManager.GetEvent(EventType.TEXTBOXSTART).action += () =>
        {
            Open();
            NextDialogue();
        };
        eventManager.GetEvent(EventType.TEXTBOXSETDATA).action += () =>
        {
            SetDialogueIDX();
        };
        eventManager.GetEvent(EventType.GAMEFAIL).action += () =>
        {
            isGameEnd = true;
            endEventType = EndType.BADENDING;
            //SetDialogueIDX();
        };
        eventManager.GetEvent(EventType.GAMESUCCESS).action += () =>
        {
            isGameEnd = true;
            endEventType = EndType.HAPPYENDING;
            SetDialogueIDX();
        };
        InputManager inputmanager = ServiceLocatorManager.GetService<InputManager>();
        inputmanager.OnClickEvent += (Vector2 pos) =>
        {
            OnNextAndStopAuto();
        };
    }
    public void TextBoxBegin()
    {
        Open();
        NextDialogue();
    }
    public void SetDialogueIDX()
    {
        DialogueData temp;
        if (isGameEnd)
            temp = dataManager.GetGameEndDialogueDataByIDX(endEventType);
        else
            temp = dataManager.GetDialogueDataByIDX(gameManager.DialogueDataCount);
        startIDX = temp.StartIDX;
        endIDX = temp.EndIDX;
        currentIDX = startIDX;
        nextDialougeIDX = temp.NextDialogueDataIDX;
    }
    public void OnNextAndStopAuto()
    {
        if (!gameObject.activeSelf)
            return;
        if (!isCoroutineActive)
        {
            if(autoPageChangeCoroutine!=null)
                StopCoroutine(autoPageChangeCoroutine);
            currentIDX++;

            if (currentIDX > endIDX)
            {
                Close();
                if (isGameEnd)
                {
                    UIManager uimanager = ServiceLocatorManager.GetService<UIManager>();
                    uimanager.GetUI<TheEndUI>(UIType.THEEND).Open();
                }
                if (nextDialougeIDX == -1)
                {
                    EventManager eventManager = ServiceLocatorManager.GetService<EventManager>();
                    eventManager.ActiveAction(EventType.GAMESUCCESS);
                    //NextDialogue();
                }
                gameManager.DialogueDataCount = nextDialougeIDX;
                return;
            }
            if (!isEvent)
                NextDialogue();
            return;
        }
        isCoroutineActive = false;
        StopCoroutine(dialogueCoroutine);
        characterDialogueTextBox.text = dialogueTextData;
        autoPageChangeCoroutine=StartCoroutine(AutoPageChange());
    }
    public void NextDialogue()
    {
        //if (!gameObject.activeSelf)
        //    Open();
        isEvent = false;
        characterNameTextBox.text = "";
        Dictionary<string, string> temp;
        if (isGameEnd)
        {
            temp = dataManager.GetEndByIDX(endEventType, currentIDX);
        }
        else
        {
            temp = dataManager.GetDialogueByIDX(currentIDX);
        }
        characterNameTextBox.text = temp["name"];
        dialogueTextData = temp["data"];
        characterDialogueTextBox.text = "";
        dialogueCoroutine = StartCoroutine(TypeOutDialogue(dialogueTextData));
       
    }
    IEnumerator AutoPageChange()
    {
        float timeTemp = nextDialogueDelay;
        while (true)
        {
            if(timeTemp <= 0) 
            {
                break;
            }
            if (!isGamePause)
                timeTemp -= Time.deltaTime;
            yield return null;
        }

        currentIDX++;
        if (currentIDX > endIDX)
        {

            if (isGameEnd)
            {
                UIManager uimanager = ServiceLocatorManager.GetService<UIManager>();
                uimanager.GetUI<TheEndUI>(UIType.THEEND).Open();
            }
          
            if (nextDialougeIDX == -1)
            {
                EventManager eventManager = ServiceLocatorManager.GetService<EventManager>();
                eventManager.ActiveAction(EventType.GAMESUCCESS);
                //NextDialogue();
            }
            gameManager.DialogueDataCount = nextDialougeIDX;
            Close();
            yield break; 
        }
        if (!isEvent)
            NextDialogue();

    }
    IEnumerator TypeOutDialogue(string data)
    {
        isCoroutineActive = true;
        int count = 0;
        while (true)
        {
            if (isGamePause)
            {
                yield return null;
                continue;
            }
            if (count == data.Length)
            {
                isCoroutineActive = false;
                break;
            }
           characterDialogueTextBox.text += data[count];
           count++;
           yield return new WaitForSeconds(typeOutDelayTime);
        }

        float timeTemp = nextDialogueDelay;
        while (true)
        {
            if (timeTemp <= 0)
            {
                break;
            }
            if (!isGamePause)
                timeTemp -= Time.deltaTime;
            yield return null;
        }

        currentIDX++;
        if (currentIDX > endIDX)
        {
            Close();
            if (isGameEnd)
            {
                UIManager uimanager = ServiceLocatorManager.GetService<UIManager>();
                uimanager.GetUI<TheEndUI>(UIType.THEEND).Open();
            }
          
            if (nextDialougeIDX == -1)
            {
                EventManager eventManager = ServiceLocatorManager.GetService<EventManager>();
                eventManager.ActiveAction(EventType.GAMESUCCESS);
                //NextDialogue();
            }
            gameManager.DialogueDataCount = nextDialougeIDX;
            yield break;
        }
        if (!isEvent)
            NextDialogue();
    }

    public void RegisterEvent(EventConnecter connecter)
    {
        
    }
}
