using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TheEndUI : UIBase
{
    [SerializeField] private float delayTime=2;
    public override void Open()
    {
        base.Open();
        Invoke("BackToMenu", delayTime);
    }
    private void BackToMenu()
    {
        Loadingmanager.LoadScene("Menu");
    }
}
