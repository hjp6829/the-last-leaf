using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TESTClickUI : UIBase
{
    private Camera cam;
    public void SetPos(Vector2 Pos)
    {
        cam = ServiceLocatorManager.GetService<CameraManager>().MainCamera;
        Pos = cam.WorldToScreenPoint(Pos);
        GetComponent<RectTransform>().position = Pos;
    }
}
