using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIBase : SerializedMonoBehaviour
{
    [SerializeField] private UIType uitype;
    [SerializeField] private bool isOpenStart;

    public bool IsOpenStart { get => isOpenStart; }
    public UIType UIType { get => uitype; }
    public virtual void Open()
    {
        gameObject.SetActive(true);
    }
    public virtual void Close()
    {
        gameObject.SetActive(false);
    }
    public virtual void initialze()
    {

    }
}
