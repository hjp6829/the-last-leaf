using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.UIElements;

public class InputManager : ManagerBase, IService
{
    [HideInInspector]
    public Action<Vector2> OnClickEvent;
    private Camera cam;
    private void Awake()
    {
        Register();
    }
    private void Start()
    {
        cam = ServiceLocatorManager.GetService<CameraManager>().MainCamera;
    }
    void OnClick(InputValue value)
    {
        Vector2 inputPosition;
#if UNITY_EDITOR

         inputPosition = Input.mousePosition;
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            inputPosition = cam.ScreenToWorldPoint(inputPosition);
            OnClickEvent?.Invoke(inputPosition);
        }


#elif UNITY_IOS || UNITY_ANDROID
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == UnityEngine.TouchPhase.Began)
            {
                inputPosition = Touchscreen.current.primaryTouch.position.ReadValue();
                if (!EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
                {
                    inputPosition = cam.ScreenToWorldPoint(inputPosition);
                    OnClickEvent?.Invoke(inputPosition);
                }
            }
        }
#endif
    }

    public void Register()
    {
        ServiceLocatorManager.ReplaceService(this);
    }

    public override void ResetAllEvent()
    {
       
    }
}
