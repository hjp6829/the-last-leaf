using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Leaf : SerializedMonoBehaviour
{
    [SerializeField]
    private SpineComponent spineComp;
    UnityEvent unityEvent;
    private void Start()
    {
        GameManager gamemanager = ServiceLocatorManager.GetService<GameManager>();
        gamemanager.OnChecngeHP += (int value) =>
        {
            SetChangeHPLeaf(value);
        };
        unityEvent = new UnityEvent();
        unityEvent.AddListener(() =>
        {
            EventManager eventmanager = ServiceLocatorManager.GetService<EventManager>();
            eventmanager.ActiveAction(EventType.GAMEFAIL);
        });
    }
    private void SetChangeHPLeaf(int value)
    {
        if (value == 0)
        {
            spineComp.SetSpineAnim("00",false, unityEvent);
            return;
        }
        if(value%2==0)
            spineComp.SetSpineAnim("0"+value.ToString(), true);
    }
}
