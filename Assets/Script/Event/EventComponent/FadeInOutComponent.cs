using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FadeInOutEventDatas
{
    public int ActiveEventIDX;
    public UnityEvent InEvent;
    public float MiddleEventDelayTime = 0;
    public UnityEvent MiddleEvent;
    public UnityEvent MiddleEndEvent;
    public UnityEvent OutEvent;
}
public class FadeInOutComponent : EventComponentBase
{
    [SerializeField]
    protected Dictionary<int, FadeInOutEventDatas> Events = new Dictionary<int, FadeInOutEventDatas>();
    [SerializeField]
    private PadeInOutEvent fadeinoutaevent;
    public override void ActiveActionByIDX(int idx)
    {
        fadeinoutaevent.ActiveEvent(idx);
    }
    public override void ActiveActionByClass(int idx)
    {
        fadeinoutaevent.ActiveEvent(Events[idx].ActiveEventIDX, Events[idx]);
    }
}
