using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class AudioEventComponent : EventComponentBase
{
    [SerializeField]
    private Dictionary<int,AudioClip> BGMClipDic = new Dictionary<int,AudioClip>();
    [SerializeField]
    private Dictionary<int, AudioClip> EffectClipDic = new Dictionary<int, AudioClip>();
    public override void ActiveActionByIDX(int idx)
    {
    }
    public override void ActiveActionByClass(int idx)
    {
    }
    public void PlayBGM(int idx)
    {
        AudioManager audiomanager = ServiceLocatorManager.GetService<AudioManager>();
        audiomanager.SetBGM(BGMClipDic[idx]);
    }
    public void PlayEffect(int idx)
    {
        AudioManager audiomanager = ServiceLocatorManager.GetService<AudioManager>();
        audiomanager.SetEffect(EffectClipDic[idx]);
    }
}
