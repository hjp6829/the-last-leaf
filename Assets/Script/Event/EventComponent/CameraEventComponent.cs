using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Rendering;

public class CameraEventComponent : EventComponentBase
{
    [SerializeField]
    protected Dictionary<int, EventComponentData> Events = new Dictionary<int, EventComponentData>();
    [SerializeField]
    private CameraEvent cameraevent;
    public override void ActiveActionByIDX(int idx)
    {
        cameraevent.ActiveEvent(idx);
    }
    public override void ActiveActionByClass(int idx)
    {
        cameraevent.ActiveEvent(Events[idx].ActiveEventIDX, Events[idx].ActiveEvent);
    }
}
