using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class SpineEventComponent : EventComponentBase
{
    [SerializeField]
    protected Dictionary<int, EventComponentData> Events = new Dictionary<int, EventComponentData>();
    [Tooltip("스킨 변환과 동시에 발생하는 이벤트들")]
    [SerializeField]
    protected Dictionary<int, EventComponentData> SkinEvents = new Dictionary<int, EventComponentData>();
    [SerializeField]
    private SpineEvent spineevent;
    public override void ActiveActionByIDX(int idx)
    {
        spineevent.AnimStart(idx);
    }
    public override void ActiveActionByClass(int idx)
    {
       spineevent.AnimStart(Events[idx].ActiveEventIDX, Events[idx].ActiveEvent);
    }
    public void ChagneSkinByIDX(int idx)
    {
        spineevent.ChangeSkin(idx);
    }
    public void ChagneSkinByClass(int idx)
    {
        spineevent.ChangeSkin(Events[idx].ActiveEventIDX, Events[idx].ActiveEvent);
    }
}
