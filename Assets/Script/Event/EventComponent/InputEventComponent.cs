using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputEventComponent : EventComponentBase
{
    [SerializeField]
    protected Dictionary<int, EventComponentData> Events = new Dictionary<int, EventComponentData>();
    [SerializeField]
    private InputEvent inputaevent;
    public override void ActiveActionByIDX(int idx)
    {
        inputaevent.ActiveEvent(idx);
    }
    public override void ActiveActionByClass(int idx)
    {
        inputaevent.ActiveEvent(Events[idx].ActiveEventIDX, Events[idx].ActiveEvent);
    }
}
