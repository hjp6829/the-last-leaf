using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class EventComponentData
{
    public int ActiveEventIDX;
    public UnityEvent ActiveEvent;
}

public abstract class EventComponentBase : SerializedMonoBehaviour
{

    public abstract void ActiveActionByIDX(int idx);
    public abstract void ActiveActionByClass(int idx);
}
