using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System;
[Serializable]
public class IDXPadeInOutEvent
{
    public float fadeInTime;
    public float fadeOutTime;
    public float delayTime = 0;
    public Color StartColor;
    public Color MiddleStartColor;
    public Color MiddleEndColor;
    public Color EndColor;
}
public class PadeInOutEvent : SerializedMonoBehaviour
{
    [SerializeField]
    private Image InOutImage;
    [FoldoutGroup("DicData")]
    [SerializeField]
    private Dictionary<int, IDXPadeInOutEvent> FadeEventDataDicBuyIDX = new Dictionary<int, IDXPadeInOutEvent>();
    private IDXPadeInOutEvent FadeInOutData;
    private FadeInOutEventDatas FadeInOutEventData;
    private UIManager uimanager;
    private bool isGamePause;
    private void Start()
    {
        GameManager gameManager = ServiceLocatorManager.GetService<GameManager>();
        gameManager.OnGamePause += (bool value) =>
        {
            isGamePause = value;
        };
        uimanager = ServiceLocatorManager.GetService<UIManager>();
    }
    public void ActiveEvent(int idx)
    {
        FadeInOutData= FadeEventDataDicBuyIDX[idx];
        StartFadeInOut();
    }
    public void ActiveEvent(int idx, FadeInOutEventDatas events)
    {
        FadeInOutData = FadeEventDataDicBuyIDX[idx];
        FadeInOutEventData = events;
        StartFadeInOut();
    }
    public void StartFadeInOut()
    {
        InOutImage.gameObject.SetActive(true);
        InOutImage.color = FadeInOutData.StartColor;
        StartCoroutine(FadeIn());
    }
    IEnumerator FadeIn()
    {
        float timeElapsed = 0;
        if(FadeInOutEventData!=null)
            FadeInOutEventData.InEvent?.Invoke();
        EventManager eventManager = ServiceLocatorManager.GetService<EventManager>();
        eventManager.ActiveAction(EventType.TEXTBOXSTOP);
        Color defaultColor = InOutImage.color;
        uimanager.OnFadeStart?.Invoke(true);
        while (true)
        {
            Color newColor = InOutImage.color;
            if (FadeInOutData.fadeInTime == 0)
            {
                newColor = FadeInOutData.MiddleStartColor;
                newColor.a = 1;
            }
            else
            {
                newColor.a = Mathf.Lerp(0, 1, timeElapsed / FadeInOutData.fadeInTime);
                newColor.r = Mathf.Lerp(defaultColor.r, FadeInOutData.MiddleStartColor.r, timeElapsed / FadeInOutData.fadeInTime);
                newColor.g = Mathf.Lerp(defaultColor.g, FadeInOutData.MiddleStartColor.g, timeElapsed / FadeInOutData.fadeInTime);
                newColor.b = Mathf.Lerp(defaultColor.b, FadeInOutData.MiddleStartColor.b, timeElapsed / FadeInOutData.fadeInTime);
            }

            InOutImage.color= newColor;
            timeElapsed += Time.deltaTime;
            if (timeElapsed >= FadeInOutData.fadeInTime)
            {
                StartCoroutine(FadeDelay());
                break;
            }
            yield return null;
        }
    }
    IEnumerator FadeDelay()
    {
        float timeElapsed = 0;
        if (FadeInOutEventData != null)
            FadeInOutEventData.MiddleEvent?.Invoke();
        Color defaultColor = InOutImage.color;
        while (true)
        {
            Color newColor = InOutImage.color;
            if (FadeInOutData.delayTime == 0)
            {
                newColor = FadeInOutData.MiddleEndColor;
                newColor.a = 1;
            }
            else
            {
                newColor.r = Mathf.Lerp(defaultColor.r, FadeInOutData.MiddleEndColor.r, timeElapsed / FadeInOutData.delayTime);
                newColor.g = Mathf.Lerp(defaultColor.g, FadeInOutData.MiddleEndColor.g, timeElapsed / FadeInOutData.delayTime);
                newColor.b = Mathf.Lerp(defaultColor.b, FadeInOutData.MiddleEndColor.b, timeElapsed / FadeInOutData.delayTime);
            }
            InOutImage.color = newColor;
            timeElapsed += Time.deltaTime;
            if (timeElapsed >= FadeInOutData.delayTime)
            {
                StartCoroutine(FadeOut());
                break;
            }
            yield return null;
        }
    }
    IEnumerator FadeOut()
    {
        float timeElapsed = 0;
        if (FadeInOutEventData != null)
            FadeInOutEventData.MiddleEndEvent?.Invoke();
        Color defaultColor = InOutImage.color;

        while (true)
        {
            Color newColor = InOutImage.color;
            if (FadeInOutData.fadeOutTime == 0)
            {
                newColor = FadeInOutData.EndColor;
                newColor.a = 0;
            }
            else
            {
                newColor.a = Mathf.Lerp(1, 0, timeElapsed / FadeInOutData.fadeOutTime);
                newColor.r = Mathf.Lerp(defaultColor.r, FadeInOutData.EndColor.r, timeElapsed / FadeInOutData.fadeOutTime);
                newColor.g = Mathf.Lerp(defaultColor.g, FadeInOutData.EndColor.g, timeElapsed / FadeInOutData.fadeOutTime);
                newColor.b = Mathf.Lerp(defaultColor.b, FadeInOutData.EndColor.b, timeElapsed / FadeInOutData.fadeOutTime);
            }

            InOutImage.color = newColor;
            timeElapsed += Time.deltaTime;
            if (timeElapsed >= FadeInOutData.fadeOutTime)
            {
                InOutImage.gameObject.SetActive(false);
                if (FadeInOutEventData != null)
                    FadeInOutEventData.OutEvent?.Invoke();
                FadeInOutEventData = null;
                uimanager.OnFadeStart?.Invoke(false);
                break;
            }
            yield return null;
        }

    }
}
