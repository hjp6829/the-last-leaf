using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CameraEventData
{
    public Transform targetPos;
    public float durationTime;
    public float size=1;
    public float delayTeim = 0;
}

public class CameraEvent : SerializedMonoBehaviour
{
    [TabGroup("DicData")]
    [SerializeField]
    private Dictionary<int, CameraEventData> CameraEventDataDic = new Dictionary<int, CameraEventData>();
    [SerializeField]
    [TabGroup("Debug")]
    private bool isDrawGizmos;
    [SerializeField]
    [TabGroup("Debug")]
    private int idx;
    [TabGroup("Debug")]
    [SerializeField]
    private EndType endType;
    private void Start()
    {
    
    }
    public void ActiveEvent(int idx,UnityEvent Event)
    {
        float delaytime = CameraEventDataDic[idx].delayTeim;
        StartCoroutine(coolTime(delaytime, () =>
        {
            CameraManager gamemanager = ServiceLocatorManager.GetService<CameraManager>();
            gamemanager.ZoomEvent(CameraEventDataDic[idx].durationTime, CameraEventDataDic[idx].targetPos.position, CameraEventDataDic[idx].size, () =>
            {
                Event?.Invoke();
            });
        }));

    }
    public void ActiveEvent(int idx)
    {
        float delaytime = CameraEventDataDic[idx].delayTeim;
        StartCoroutine(coolTime(delaytime, () =>
        {
            CameraManager gamemanager = ServiceLocatorManager.GetService<CameraManager>();
            gamemanager.ZoomEvent(CameraEventDataDic[idx].durationTime, CameraEventDataDic[idx].targetPos.position, CameraEventDataDic[idx].size, () =>
            {
              
            });
        }));
    }
    private void OnDrawGizmos()
    {
        if (!isDrawGizmos)
            return;
        if (!CameraEventDataDic.ContainsKey(idx))
            return;
        float width= Screen.width;
        float height = Screen.height;
        float ratio = width / height;
        float size = CameraEventDataDic[idx].size;
        float cameraHeight = 2 * size;
        float cameraWidth = cameraHeight* ratio;
        Gizmos.color = Color.blue;
        Vector2 centerPos = CameraEventDataDic[idx].targetPos.position;

        Gizmos.DrawWireCube(centerPos,new Vector2(cameraWidth, cameraHeight));
    }
    IEnumerator coolTime(float time,Action action)
    {
        while (true)
        {
            if(time<=0)
            {
                action?.Invoke();
                break;
            }
            time -= Time.deltaTime;
            yield return null;
        }
    }
}
