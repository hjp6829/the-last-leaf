using Sirenix.OdinInspector;
using Spine.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class AnimData
{
    public string name;
    public bool isLoop;
}
public class SpineEvent : SerializedMonoBehaviour
{
    [SerializeField]
    private Dictionary<int, AnimData> SpineAnimEventDic = new Dictionary<int, AnimData>();
    [SerializeField]
    private Dictionary<int, string> SpineSkinEventDic = new Dictionary<int, string>();
    private SpineComponent spineComp;
    private void Start()
    {
        spineComp=GetComponent<SpineComponent>();
    }
    public void AnimStart(int idx)
    {
        AnimData data = SpineAnimEventDic[idx];
        spineComp.SetSpineAnim(data.name, data.isLoop);
    }
    public void AnimStart(int idx,UnityEvent Event)
    {
        AnimData data = SpineAnimEventDic[idx];
        spineComp.SetSpineAnim(data.name, data.isLoop, Event);
    }
    public void ChangeSkin(int idx)
    {
        string name = SpineSkinEventDic[idx];
        spineComp.SetSkin(name);
    }
    public void ChangeSkin(int idx, UnityEvent Event)
    {
        string name = SpineSkinEventDic[idx];
        spineComp.SetSkin(name);
        Event?.Invoke();
    }
    public void SetSkin(string skinname)
    {
        spineComp.SetSkin(skinname);
    }
    //public void ChangeSkeleton(SkeletonDataAsset skeletonData)
    //{
    //    spineComp.SetSpine(skeletonData);
    //}
}
