using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InputEventData
{
    public Transform Pos;
    public float Radious;
}

public class InputEvent : SerializedMonoBehaviour
{
    [SerializeField]
    private Dictionary<int, InputEventData> InputEventDataDic = new Dictionary<int, InputEventData>();
    private bool isEvent;
    private float eventRadious;
    private Vector2 eventPos;
    private UnityEvent clickEvent;
    private void Start()
    {
        InputManager inputManager = ServiceLocatorManager.GetService<InputManager>();
        inputManager.OnClickEvent += (Vector2 pos) =>
        {
            OnClickEvent(pos);
        };
    }
    public void ActiveEvent(int idx)
    {
        EventManager eventManager = ServiceLocatorManager.GetService<EventManager>();
        eventManager.ActiveAction(EventType.TEXTBOXSTOP);
        makeInputPos(idx);
    }
    public void ActiveEvent(int idx, UnityEvent Event)
    {
        clickEvent = Event;
        EventManager eventManager = ServiceLocatorManager.GetService<EventManager>();
        eventManager.ActiveAction(EventType.TEXTBOXSTOP);
        makeInputPos(idx);
    }
    void makeInputPos(int idx)
    {
        isEvent = true;
        eventRadious = InputEventDataDic[idx].Radious;
        eventPos = InputEventDataDic[idx].Pos.position;
        TESTClickUI clickui = ServiceLocatorManager.GetService<UIManager>().GetUI<TESTClickUI>(UIType.TESTCLICK);
        clickui.SetPos(eventPos);
        clickui.Open();
    }
    void OnClickEvent(Vector2 pos)
    {
        if (!isEvent)
            return;
        float dis = Vector2.Distance(eventPos, pos);
        if (dis <= eventRadious)
        {
            isEvent = false;
            clickEvent?.Invoke();
            TESTClickUI clickui = ServiceLocatorManager.GetService<UIManager>().GetUI<TESTClickUI>(UIType.TESTCLICK);
            clickui.Close();
        }
    }
    private void OnDrawGizmos()
    {
        if (!isEvent)
            return;
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(eventPos, eventRadious);
    }
}
