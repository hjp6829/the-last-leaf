using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameCamera : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        CameraManager cameramanager = ServiceLocatorManager.GetService<CameraManager>();
        cameramanager.MainCamera = GetComponent<Camera>();
    }
}
