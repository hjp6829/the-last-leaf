using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class MiniGame2 : UIBase
{
    [TabGroup("Objects")]
    [SerializeField]
    private Slider slider;
    [TabGroup("Objects")]
    [SerializeField]
    private TextMeshProUGUI timeLimitText;
    [TabGroup("Objects")]
    [SerializeField]
    private TextMeshProUGUI StartTimeText;
    [TabGroup("Objects")]
    [SerializeField]
    private GameObject BackGround;
    [TabGroup("Objects")]
    [SerializeField]
    private GameObject SuccessText;
    [TabGroup("Objects")]
    [SerializeField]
    private GameObject FailText;
    [TabGroup("Objects")]
    [SerializeField]
    private List<GameObject> HPImageList;
    [TabGroup("Data")]
    [Range(0.01f,0.05f)]
    [SerializeField]
    private float decreaseValue;
    [TabGroup("Data")]
    [SerializeField]
    private float increaseValue;
    private Coroutine decreaseCoroutine;
    [TabGroup("Data")]
    [SerializeField]
    private float timeLimit = 10;
    [TabGroup("Data")]
    [SerializeField]
    private float startCoolTime = 3.0f;
    [TabGroup("Action")]
    [SerializeField]
    private UnityEvent gameSuccessAction;
    [TabGroup("Action")]
    [SerializeField]
    private UnityEvent gameFailAction;
    [TabGroup("Action")]
    [SerializeField]
    private UnityEvent endAction;
    private float currentTimeLimit;
    private int loofCount=3;
    private bool isGamePause;
    private bool isGameEnd;
    public override void initialze()
    {
        EventManager eventmanager = ServiceLocatorManager.GetService<EventManager>();
        eventmanager.GetEvent(EventType.MINIGAME1START).action += () =>
        {
            GameStart();
        };
        eventmanager.GetEvent(EventType.GAMEFAIL).action += () =>
        {
            if (gameObject.activeSelf)
            {
                isGameEnd = true;
                GameEnd();
                endAction?.Invoke();
            }
        };
        GameManager gamemanager = ServiceLocatorManager.GetService<GameManager>();
        gamemanager.OnGamePause += (bool value) =>
        {
            isGamePause = value;
        };
    }

    public override void Open()
    {
        base.Open();
        GameManager gamemanager = ServiceLocatorManager.GetService<GameManager>();
        gamemanager.HPUpdate();

    }
    public override void Close()
    {
        base.Close();
        foreach(var Object in HPImageList)
        {
            Object.SetActive(true);
        }
        BackGround.SetActive(true);
        loofCount = 3;
        slider.value = 0;
        isGameEnd = false;
        SuccessText.SetActive(false);
        FailText.SetActive(false);
    }
    public void GameStart()
    {
        Open();
        slider.value = 0;
        StartTimeText.gameObject.SetActive(true);
        currentTimeLimit = timeLimit;
        timeLimitText.text = ((int)timeLimit).ToString();
        StartCoroutine(StartCoolTimeUpdate());
    }
    public void GameEnd()
    {
        StopCoroutine(decreaseCoroutine);
        isGameEnd = true;
        //Close();
        //EventManager eventmanager = ServiceLocatorManager.GetService<EventManager>();
        //eventmanager.ActiveAction(EventType.TEXTBOXSETDATA);
        //eventmanager.ActiveAction(EventType.TEXTBOXSTART);
    }
    public void IncreaseSlider()
    {
        if (isGamePause|| isGameEnd)
        {
            return;
        }
        slider.value += increaseValue;
        if (slider.value>=100)
        {
            SuccessText.SetActive(true);
            gameSuccessAction?.Invoke();
            GameEnd();
        }
    }
    IEnumerator StartCoolTimeUpdate()
    {
        float tempTime = startCoolTime;
        while (true)
        {
            if (isGamePause)
            {
                yield return null;
                continue;
            }
            if (tempTime <= 0)
            {
                currentTimeLimit = timeLimit;
                timeLimitText.text = ((int)timeLimit).ToString();
                decreaseCoroutine = StartCoroutine(DecreaseSlider());
                StartTimeText.gameObject.SetActive(false);
                break;
            }
            StartTimeText.text = ((int)tempTime+1).ToString();
            tempTime -= Time.deltaTime;
            yield return null;
        }
    }
    IEnumerator DecreaseSlider()
    {
        while(true)
        {
            if (isGamePause)
            {
                yield return null;
                continue;
            }
            if (currentTimeLimit <= 0)
            {
                GameManager gamemanager = ServiceLocatorManager.GetService<GameManager>();
                gamemanager.LeafHP -= 1;
                if(gamemanager.LeafHP <= 0 )
                {
                    BackGround.SetActive(false);
                    break;
                }
                HPImageList[loofCount - 1].SetActive(false);
                loofCount--;
                if(loofCount==0)
                {
                    FailText.SetActive(true);
                    gameFailAction?.Invoke();
                    GameEnd();
                    break;
                }
                else
                {
                    GameStart();
                    break;
                }
            }
            if(slider.value>0)
            {
                slider.value -= decreaseValue*Time.deltaTime*1000;
            }
            currentTimeLimit -= Time.deltaTime;
            timeLimitText.text = ((int)currentTimeLimit).ToString();
            yield return null;
        }
    }
}
