using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class MiniGame : UIBase
{
    [TabGroup("object")]
    [SerializeField]
    private RectTransform SliderRectTransform;
    [TabGroup("object")]
    [SerializeField]
    private RectTransform BarRectTransform;
    [TabGroup("object")]
    [SerializeField]
    private RectTransform TargetZoneRectTransform;
    [TabGroup("object")]
    [SerializeField]
    private TextMeshProUGUI StartTimeText;
    [TabGroup("object")]
    [SerializeField]
    private TextMeshProUGUI GameTimeText;
    [TabGroup("object")]
    [SerializeField]
    private GameObject BackGround;
    [TabGroup("object")]
    [SerializeField]
    private GameObject SuccessText;
    [TabGroup("object")]
    [SerializeField]
    private GameObject FailText;
    [TabGroup("object")]
    [SerializeField]
    private List<GameObject> HPImageList;
    [TabGroup("data")]
    [SerializeField]
    private float SliderSpeed = 1;
    [TabGroup("data")]
    [SerializeField]
    private float TargetZoneSizeMin;
    [TabGroup("data")]
    [SerializeField]
    private float TargetZoneSizeMax;
    [TabGroup("Action")]
    [SerializeField]
    private UnityEvent gameSuccessAction;
    [TabGroup("Action")]
    [SerializeField]
    private UnityEvent gameFailAction;
    [TabGroup("Action")]
    [SerializeField]
    private UnityEvent endAction;

    private float barLeftSize;
    private float barRightSize;
    private float TargetZoneLeft;
    private float TargetZoneRight;
    private Vector3 moveVector;
    [SerializeField]
    private float startCoolTime=3.0f;
    private Coroutine gameUpdateRoutine;
    [SerializeField]
    private int maxConut=3;
    private int currentCount = 0;
    [SerializeField]
    private int maxHP=3;
    private int currentHP;
    [SerializeField]
    private float gameTime = 10;
    private bool isGamePause;
    public override void Open()
    {
        base.Open();
        GameManager gamemanager = ServiceLocatorManager.GetService<GameManager>();
        gamemanager.HPUpdate();
    }
    public override void Close()
    {
        base.Close();
        foreach (var Object in HPImageList)
        {
            Object.SetActive(true);
        }
        BackGround.SetActive(true);
        SuccessText.SetActive(false);
        FailText.SetActive(false);
        Vector3 sliderPos = SliderRectTransform.localPosition;
        sliderPos.x = barLeftSize;
        SliderRectTransform.localPosition = sliderPos;
    }

    public void GameStart()
    {
        Open();
        StartTimeText.gameObject.SetActive(true);
        StartTimeText.text = ((int)startCoolTime).ToString();
        GameTimeText.text = ((int)gameTime).ToString();
        StartCoroutine(StartCoolTimeUpdate());
    }
    private void MakeNewClickSpace()
    {
        float TargetZoneRandomSize = Random.Range(TargetZoneSizeMin, TargetZoneSizeMax);
        TargetZoneRectTransform.sizeDelta = new Vector2(TargetZoneRandomSize, TargetZoneRectTransform.sizeDelta.y);

        float TargetZoneMiddlePosLeft = barLeftSize + (TargetZoneRandomSize * 0.5f);
        float TargetZoneMiddlePosRight = barRightSize - (TargetZoneRandomSize * 0.5f);
        float TargetZoneRandomPosX = Random.Range(TargetZoneMiddlePosLeft, TargetZoneMiddlePosRight);
        TargetZoneRectTransform.localPosition = new Vector3(TargetZoneRandomPosX, 0, 0);

        TargetZoneLeft = TargetZoneRectTransform.localPosition.x - (TargetZoneRectTransform.sizeDelta.x * 0.5f);
        TargetZoneRight = TargetZoneRectTransform.localPosition.x + (TargetZoneRectTransform.sizeDelta.x * 0.5f);
    }
    public void GameEnd()
    {
        if(gameUpdateRoutine!=null)
            StopCoroutine(gameUpdateRoutine);

        //Close();
    }
    IEnumerator StartCoolTimeUpdate()
    {
        float tempTime = startCoolTime;
        while (true)
        {
            if (isGamePause)
            {
                yield return null;
                continue;
            }
            if (tempTime <= 0)
            {
               
                currentHP = maxHP;
                currentCount = 0;
                moveVector = Vector3.right;
                barLeftSize = (BarRectTransform.sizeDelta.x * 0.5f) * -1;
                barRightSize = (BarRectTransform.sizeDelta.x * 0.5f);
                MakeNewClickSpace();
                gameUpdateRoutine = StartCoroutine(GameUpdate());
                StartTimeText.gameObject.SetActive(false);
                break;
            }
            tempTime -= Time.deltaTime;
            StartTimeText.text = ((int)tempTime+1).ToString();
            yield return null;
        }
    }
    IEnumerator GameUpdate()
    {
        float tempGameTime = gameTime;
        while (true)
        {
            if(isGamePause)
            {
                yield return null;
                continue;
            }
            if (tempGameTime <= 0)
            {
                GameManager gamemanager = ServiceLocatorManager.GetService<GameManager>();
                gamemanager.LeafHP -= maxHP;
                if (gamemanager.LeafHP > 0)
                {
                    FailText.SetActive(true);
                    gameFailAction?.Invoke();
                }
                GameEnd();
                break;
            }
            if (SliderRectTransform.localPosition.x <= barLeftSize)
                moveVector = Vector3.right;
            else if (SliderRectTransform.localPosition.x >= barRightSize)
                moveVector = Vector3.left;
            SliderRectTransform.position += moveVector * SliderSpeed*Time.deltaTime;
            tempGameTime -= Time.deltaTime;
            GameTimeText.text = ((int)tempGameTime + 1).ToString();
            yield return null;
        }
    }
    public override void initialze()
    {
        EventManager eventmanager = ServiceLocatorManager.GetService<EventManager>();
        eventmanager.GetEvent(EventType.GAMEFAIL).action += () =>
        {
            if (gameObject.activeSelf)
            {
                GameEnd();
                endAction?.Invoke();
            }
        };
        GameManager gamemanager = ServiceLocatorManager.GetService<GameManager>();
        gamemanager.OnGamePause += (bool value) =>
        {
            isGamePause = value;
        };
    }
    public void ClickBar()
    {
        GameManager gamemanager = ServiceLocatorManager.GetService<GameManager>();
        if (gamemanager.LeafHP == 0)
            return;
        if(SliderRectTransform.localPosition.x>= TargetZoneLeft && SliderRectTransform.localPosition.x<= TargetZoneRight)
        {
            currentCount++;
            MakeNewClickSpace();
            if (maxConut == currentCount)
            {
                SuccessText.SetActive(true);
                gameSuccessAction?.Invoke();
                GameEnd();
                //EventManager eventmanager = ServiceLocatorManager.GetService<EventManager>();
                //eventmanager.ActiveAction(EventType.TEXTBOXSETDATA);
                //eventmanager.ActiveAction(EventType.TEXTBOXSTART);
            }
            return;
        }
        HPImageList[currentHP - 1].SetActive(false);
        currentHP--;
        gamemanager.LeafHP -= 1;
        if (gamemanager.LeafHP==0)
        {
            if (gameUpdateRoutine != null)
                StopCoroutine(gameUpdateRoutine);
            BackGround.SetActive(false);
        }
        if (currentHP <= 0)
        {
            if (gamemanager.LeafHP > 0)
            {
                FailText.SetActive(true);
                gameFailAction?.Invoke();
            }
            GameEnd();
        }
    }
}
