
Character.png
size: 4096,4096
format: RGBA8888
filter: Linear,Linear
repeat: none
02
  rotate: false
  xy: 2300, 3965
  size: 90, 85
  orig: 92, 97
  offset: 1, 11
  index: -1
10
  rotate: false
  xy: 2392, 3965
  size: 95, 85
  orig: 97, 87
  offset: 1, 1
  index: -1
18
  rotate: false
  xy: 2977, 3960
  size: 90, 85
  orig: 92, 87
  offset: 1, 1
  index: -1
After02
  rotate: false
  xy: 3069, 3957
  size: 90, 85
  orig: 92, 97
  offset: 1, 11
  index: -1
After10
  rotate: false
  xy: 3405, 3952
  size: 95, 85
  orig: 97, 87
  offset: 1, 1
  index: -1
After18
  rotate: false
  xy: 3502, 3952
  size: 90, 85
  orig: 92, 87
  offset: 1, 1
  index: -1
After22
  rotate: true
  xy: 4004, 3937
  size: 95, 90
  orig: 97, 97
  offset: 1, 6
  index: -1
After걷힌커튼
  rotate: true
  xy: 1387, 3863
  size: 100, 1000
  orig: 102, 1002
  offset: 1, 1
  index: -1
After꽃
  rotate: true
  xy: 623, 3895
  size: 102, 122
  orig: 102, 122
  offset: 0, 0
  index: -1
After눈
  rotate: true
  xy: 3194, 4039
  size: 55, 60
  orig: 57, 62
  offset: 1, 1
  index: -1
After눈감음
  rotate: true
  xy: 943, 3995
  size: 65, 70
  orig: 67, 72
  offset: 1, 1
  index: -1
After눈섭
  rotate: false
  xy: 2, 4084
  size: 35, 10
  orig: 37, 12
  offset: 1, 1
  index: -1
After닫힌커튼
  rotate: true
  xy: 2166, 1531
  size: 477, 1233
  orig: 477, 1233
  offset: 0, 0
  index: -1
After더듬이
  rotate: false
  xy: 2288, 4052
  size: 67, 42
  orig: 67, 42
  offset: 0, 0
  index: -1
After뒷머리
  rotate: true
  xy: 3018, 3231
  size: 269, 292
  orig: 272, 292
  offset: 0, 0
  index: -1
After배게
  rotate: true
  xy: 2, 2796
  size: 340, 350
  orig: 342, 467
  offset: 1, 1
  index: -1
After사이드머리
  rotate: true
  xy: 2495, 4047
  size: 47, 212
  orig: 47, 212
  offset: 0, 0
  index: -1
After상체
  rotate: true
  xy: 311, 3378
  size: 227, 387
  orig: 227, 387
  offset: 0, 0
  index: -1
After색연필
  rotate: true
  xy: 3819, 3462
  size: 190, 270
  orig: 192, 272
  offset: 1, 1
  index: -1
After시계1
  rotate: false
  xy: 1319, 4059
  size: 40, 35
  orig: 42, 37
  offset: 1, 1
  index: -1
After앞머리
  rotate: true
  xy: 3686, 3950
  size: 87, 157
  orig: 87, 157
  offset: 0, 0
  index: -1
After얼굴
  rotate: false
  xy: 768, 3604
  size: 165, 155
  orig: 167, 157
  offset: 1, 1
  index: -1
After옆머리
  rotate: true
  xy: 161, 3914
  size: 97, 152
  orig: 97, 152
  offset: 0, 0
  index: -1
After오른손
  rotate: true
  xy: 1159, 3990
  size: 70, 91
  orig: 72, 93
  offset: 1, 1
  index: -1
After오른팔
  rotate: true
  xy: 2601, 3525
  size: 182, 277
  orig: 182, 277
  offset: 0, 0
  index: -1
After왼사이드머리
  rotate: true
  xy: 3161, 3704
  size: 147, 172
  orig: 147, 172
  offset: 0, 0
  index: -1
After왼손
  rotate: false
  xy: 3940, 4034
  size: 75, 60
  orig: 77, 62
  offset: 1, 1
  index: -1
After왼팔
  rotate: true
  xy: 2, 3405
  size: 233, 307
  orig: 237, 307
  offset: 0, 0
  index: -1
After의자
  rotate: true
  xy: 1688, 3720
  size: 141, 300
  orig: 143, 302
  offset: 1, 1
  index: -1
After이불
  rotate: false
  xy: 3047, 2447
  size: 890, 425
  orig: 892, 912
  offset: 1, 1
  index: -1
After침대파츠
  rotate: false
  xy: 1786, 2098
  size: 1080, 435
  orig: 1082, 437
  offset: 1, 1
  index: -1
After크로키북
  rotate: false
  xy: 439, 3763
  size: 245, 130
  orig: 247, 132
  offset: 1, 1
  index: -1
After탁자
  rotate: false
  xy: 1058, 2702
  size: 512, 347
  orig: 514, 349
  offset: 1, 1
  index: -1
After홍조
  rotate: false
  xy: 776, 4064
  size: 40, 30
  orig: 42, 32
  offset: 1, 1
  index: -1
Dinner30
  rotate: false
  xy: 3594, 3952
  size: 90, 85
  orig: 92, 87
  offset: 1, 1
  index: -1
DinnerLayer 11
  rotate: false
  xy: 1361, 4059
  size: 40, 35
  orig: 42, 37
  offset: 1, 1
  index: -1
Dinner걷힌커튼
  rotate: true
  xy: 2389, 3853
  size: 100, 1000
  orig: 102, 1002
  offset: 1, 1
  index: -1
Dinner꽃
  rotate: true
  xy: 747, 3895
  size: 102, 122
  orig: 102, 122
  offset: 0, 0
  index: -1
Dinner눈
  rotate: true
  xy: 3256, 4039
  size: 55, 60
  orig: 57, 62
  offset: 1, 1
  index: -1
Dinner눈감음
  rotate: true
  xy: 1015, 3995
  size: 65, 70
  orig: 67, 72
  offset: 1, 1
  index: -1
Dinner눈섭
  rotate: false
  xy: 39, 4084
  size: 35, 10
  orig: 37, 12
  offset: 1, 1
  index: -1
Dinner더듬이
  rotate: false
  xy: 2357, 4052
  size: 67, 42
  orig: 67, 42
  offset: 0, 0
  index: -1
Dinner뒷머리
  rotate: true
  xy: 3312, 3204
  size: 269, 292
  orig: 272, 292
  offset: 0, 0
  index: -1
Dinner배게
  rotate: true
  xy: 354, 2796
  size: 340, 350
  orig: 342, 467
  offset: 1, 1
  index: -1
Dinner사이드머리
  rotate: true
  xy: 2709, 4047
  size: 47, 212
  orig: 47, 212
  offset: 0, 0
  index: -1
Dinner상체
  rotate: true
  xy: 700, 3375
  size: 227, 387
  orig: 227, 387
  offset: 0, 0
  index: -1
Dinner앞머리
  rotate: true
  xy: 3845, 3945
  size: 87, 157
  orig: 87, 157
  offset: 0, 0
  index: -1
Dinner얼굴
  rotate: false
  xy: 935, 3604
  size: 165, 155
  orig: 167, 157
  offset: 1, 1
  index: -1
Dinner옆머리
  rotate: true
  xy: 315, 3911
  size: 97, 152
  orig: 97, 152
  offset: 0, 0
  index: -1
Dinner오른손
  rotate: true
  xy: 1252, 3987
  size: 70, 91
  orig: 72, 93
  offset: 1, 1
  index: -1
Dinner오른팔
  rotate: true
  xy: 2880, 3525
  size: 182, 277
  orig: 182, 277
  offset: 0, 0
  index: -1
Dinner왼사이드머리
  rotate: true
  xy: 3335, 3686
  size: 147, 172
  orig: 147, 172
  offset: 0, 0
  index: -1
Dinner왼손
  rotate: false
  xy: 4017, 4034
  size: 75, 60
  orig: 77, 62
  offset: 1, 1
  index: -1
Dinner왼팔
  rotate: true
  xy: 1478, 3313
  size: 233, 307
  orig: 237, 307
  offset: 0, 0
  index: -1
Dinner의자
  rotate: true
  xy: 1990, 3720
  size: 141, 300
  orig: 143, 302
  offset: 1, 1
  index: -1
Dinner이불
  rotate: false
  xy: 2, 2322
  size: 890, 425
  orig: 892, 912
  offset: 1, 1
  index: -1
Dinner침대파츠
  rotate: false
  xy: 2868, 2010
  size: 1080, 435
  orig: 1082, 437
  offset: 1, 1
  index: -1
Dinner탁자
  rotate: false
  xy: 1572, 2702
  size: 512, 347
  orig: 514, 349
  offset: 1, 1
  index: -1
Dinner홍조
  rotate: false
  xy: 818, 4064
  size: 40, 30
  orig: 42, 32
  offset: 1, 1
  index: -1
Ladder
  rotate: true
  xy: 2600, 2535
  size: 405, 445
  orig: 407, 447
  offset: 1, 1
  index: -1
Layer 11
  rotate: false
  xy: 1403, 4059
  size: 40, 35
  orig: 42, 37
  offset: 1, 1
  index: -1
Layer 12
  rotate: false
  xy: 3137, 4044
  size: 55, 50
  orig: 57, 52
  offset: 1, 1
  index: -1
Layer 13
  rotate: true
  xy: 3318, 4039
  size: 55, 60
  orig: 57, 62
  offset: 1, 1
  index: -1
Layer 14
  rotate: false
  xy: 3380, 4039
  size: 55, 55
  orig: 57, 57
  offset: 1, 1
  index: -1
Leaf
  rotate: true
  xy: 1687, 3971
  size: 84, 113
  orig: 97, 120
  offset: 8, 2
  index: -1
Light
  rotate: true
  xy: 2, 3794
  size: 115, 435
  orig: 117, 437
  offset: 1, 1
  index: -1
WallLeaf
  rotate: true
  xy: 3637, 2874
  size: 315, 390
  orig: 317, 392
  offset: 1, 1
  index: -1
걷힌커튼
  rotate: true
  xy: 686, 3761
  size: 100, 1000
  orig: 102, 1002
  offset: 1, 1
  index: -1
그림자조명
  rotate: true
  xy: 3401, 928
  size: 1080, 650
  orig: 1082, 662
  offset: 1, 1
  index: -1
꽃
  rotate: true
  xy: 871, 3891
  size: 102, 122
  orig: 102, 122
  offset: 0, 0
  index: -1
눈
  rotate: true
  xy: 3437, 4039
  size: 55, 60
  orig: 57, 62
  offset: 1, 1
  index: -1
눈감음
  rotate: true
  xy: 1087, 3995
  size: 65, 70
  orig: 67, 72
  offset: 1, 1
  index: -1
눈섭
  rotate: false
  xy: 76, 4084
  size: 35, 10
  orig: 37, 12
  offset: 1, 1
  index: -1
닝겐2
  rotate: true
  xy: 3438, 3475
  size: 185, 379
  orig: 188, 381
  offset: 1, 1
  index: -1
닫힌커튼
  rotate: true
  xy: 1044, 1052
  size: 477, 1233
  orig: 477, 1233
  offset: 0, 0
  index: -1
더듬이
  rotate: false
  xy: 2426, 4052
  size: 67, 42
  orig: 67, 42
  offset: 0, 0
  index: -1
뒷머리
  rotate: true
  xy: 3606, 3191
  size: 269, 292
  orig: 272, 292
  offset: 0, 0
  index: -1
바리1After감은눈
  rotate: false
  xy: 1445, 4057
  size: 137, 37
  orig: 137, 37
  offset: 0, 0
  index: -1
바리1After눈섭
  rotate: false
  xy: 113, 4077
  size: 112, 17
  orig: 112, 17
  offset: 0, 0
  index: -1
바리1After더듬이
  rotate: false
  xy: 79, 4013
  size: 62, 62
  orig: 62, 62
  offset: 0, 0
  index: -1
바리1After뒷머리
  rotate: false
  xy: 924, 3091
  size: 300, 282
  orig: 302, 282
  offset: 2, 0
  index: -1
바리1After뜬눈
  rotate: false
  xy: 143, 4013
  size: 152, 62
  orig: 152, 62
  offset: 0, 0
  index: -1
바리1After사이드머리
  rotate: true
  xy: 1438, 3973
  size: 82, 247
  orig: 82, 247
  offset: 0, 0
  index: -1
바리1After앞머리
  rotate: false
  xy: 3852, 3654
  size: 212, 152
  orig: 212, 152
  offset: 0, 0
  index: -1
바리1After얼굴
  rotate: false
  xy: 3683, 3662
  size: 167, 152
  orig: 167, 152
  offset: 0, 0
  index: -1
바리1After옆머리
  rotate: true
  xy: 2594, 3709
  size: 142, 187
  orig: 142, 187
  offset: 0, 0
  index: -1
바리1After홍조
  rotate: false
  xy: 902, 4062
  size: 137, 32
  orig: 137, 32
  offset: 0, 0
  index: -1
바리1Dinner감은눈
  rotate: false
  xy: 1584, 4057
  size: 137, 37
  orig: 137, 37
  offset: 0, 0
  index: -1
바리1Dinner눈섭
  rotate: false
  xy: 227, 4077
  size: 112, 17
  orig: 112, 17
  offset: 0, 0
  index: -1
바리1Dinner더듬이
  rotate: false
  xy: 297, 4013
  size: 62, 62
  orig: 62, 62
  offset: 0, 0
  index: -1
바리1Dinner뒷머리
  rotate: false
  xy: 2148, 3004
  size: 300, 282
  orig: 302, 282
  offset: 2, 0
  index: -1
바리1Dinner뜬눈
  rotate: false
  xy: 425, 4010
  size: 152, 62
  orig: 152, 62
  offset: 0, 0
  index: -1
바리1Dinner사이드머리
  rotate: true
  xy: 1802, 3970
  size: 82, 247
  orig: 82, 247
  offset: 0, 0
  index: -1
바리1Dinner앞머리
  rotate: false
  xy: 2, 3640
  size: 212, 152
  orig: 212, 152
  offset: 0, 0
  index: -1
바리1Dinner얼굴
  rotate: false
  xy: 216, 3640
  size: 167, 152
  orig: 167, 152
  offset: 0, 0
  index: -1
바리1Dinner옆머리
  rotate: true
  xy: 2783, 3709
  size: 142, 187
  orig: 142, 187
  offset: 0, 0
  index: -1
바리1Dinner홍조
  rotate: false
  xy: 1041, 4062
  size: 137, 32
  orig: 137, 32
  offset: 0, 0
  index: -1
바리1감은눈
  rotate: false
  xy: 1723, 4057
  size: 137, 37
  orig: 137, 37
  offset: 0, 0
  index: -1
바리1눈섭
  rotate: false
  xy: 341, 4077
  size: 112, 17
  orig: 112, 17
  offset: 0, 0
  index: -1
바리1더듬이
  rotate: false
  xy: 361, 4013
  size: 62, 62
  orig: 62, 62
  offset: 0, 0
  index: -1
바리1뒷머리
  rotate: false
  xy: 2450, 3004
  size: 300, 282
  orig: 302, 282
  offset: 2, 0
  index: -1
바리1뜬눈
  rotate: false
  xy: 579, 4010
  size: 152, 62
  orig: 152, 62
  offset: 0, 0
  index: -1
바리1사이드머리
  rotate: true
  xy: 2051, 3968
  size: 82, 247
  orig: 82, 247
  offset: 0, 0
  index: -1
바리1앞머리
  rotate: false
  xy: 385, 3609
  size: 212, 152
  orig: 212, 152
  offset: 0, 0
  index: -1
바리1얼굴
  rotate: false
  xy: 599, 3607
  size: 167, 152
  orig: 167, 152
  offset: 0, 0
  index: -1
바리1옆머리
  rotate: true
  xy: 2972, 3709
  size: 142, 187
  orig: 142, 187
  offset: 0, 0
  index: -1
바리1홍조
  rotate: false
  xy: 1180, 4062
  size: 137, 32
  orig: 137, 32
  offset: 0, 0
  index: -1
바리2After감은눈
  rotate: false
  xy: 1862, 4054
  size: 140, 40
  orig: 142, 42
  offset: 1, 1
  index: -1
바리2After눈섭
  rotate: false
  xy: 455, 4074
  size: 105, 20
  orig: 107, 22
  offset: 1, 1
  index: -1
바리2After더듬이
  rotate: false
  xy: 733, 3999
  size: 68, 63
  orig: 68, 63
  offset: 0, 0
  index: -1
바리2After뒷머리
  rotate: false
  xy: 2752, 2942
  size: 293, 287
  orig: 297, 287
  offset: 4, 0
  index: -1
바리2After뜬눈
  rotate: false
  xy: 3499, 4039
  size: 145, 55
  orig: 147, 57
  offset: 1, 1
  index: -1
바리2After사이드머리
  rotate: true
  xy: 2489, 3963
  size: 82, 242
  orig: 82, 242
  offset: 0, 0
  index: -1
바리2After앞머리
  rotate: false
  xy: 995, 3873
  size: 194, 115
  orig: 194, 116
  offset: 0, 1
  index: -1
바리2After얼굴
  rotate: false
  xy: 1102, 3604
  size: 160, 155
  orig: 162, 157
  offset: 1, 1
  index: -1
바리2After옆머리
  rotate: true
  xy: 3587, 3821
  size: 127, 151
  orig: 127, 156
  offset: 0, 5
  index: -1
바리2Dinner감은눈
  rotate: false
  xy: 2004, 4054
  size: 140, 40
  orig: 142, 42
  offset: 1, 1
  index: -1
바리2Dinner눈섭
  rotate: false
  xy: 562, 4074
  size: 105, 20
  orig: 107, 22
  offset: 1, 1
  index: -1
바리2Dinner더듬이
  rotate: false
  xy: 803, 3999
  size: 68, 63
  orig: 68, 63
  offset: 0, 0
  index: -1
바리2Dinner뒷머리
  rotate: false
  xy: 3047, 2915
  size: 293, 287
  orig: 297, 287
  offset: 4, 0
  index: -1
바리2Dinner뜬눈
  rotate: false
  xy: 3646, 4039
  size: 145, 55
  orig: 147, 57
  offset: 1, 1
  index: -1
바리2Dinner사이드머리
  rotate: true
  xy: 2733, 3963
  size: 82, 242
  orig: 82, 242
  offset: 0, 0
  index: -1
바리2Dinner앞머리
  rotate: false
  xy: 1191, 3870
  size: 194, 115
  orig: 194, 116
  offset: 0, 1
  index: -1
바리2Dinner얼굴
  rotate: false
  xy: 1264, 3604
  size: 160, 155
  orig: 162, 157
  offset: 1, 1
  index: -1
바리2Dinner옆머리
  rotate: true
  xy: 3740, 3816
  size: 127, 151
  orig: 127, 156
  offset: 0, 5
  index: -1
바리2감은눈
  rotate: false
  xy: 2146, 4054
  size: 140, 40
  orig: 142, 42
  offset: 1, 1
  index: -1
바리2눈섭
  rotate: false
  xy: 669, 4074
  size: 105, 20
  orig: 107, 22
  offset: 1, 1
  index: -1
바리2더듬이
  rotate: false
  xy: 873, 3997
  size: 68, 63
  orig: 68, 63
  offset: 0, 0
  index: -1
바리2뒷머리
  rotate: false
  xy: 3342, 2902
  size: 293, 287
  orig: 297, 287
  offset: 4, 0
  index: -1
바리2뜬눈
  rotate: false
  xy: 3793, 4039
  size: 145, 55
  orig: 147, 57
  offset: 1, 1
  index: -1
바리2사이드머리
  rotate: true
  xy: 3161, 3955
  size: 82, 242
  orig: 82, 242
  offset: 0, 0
  index: -1
바리2앞머리
  rotate: false
  xy: 3391, 3835
  size: 194, 115
  orig: 194, 116
  offset: 0, 1
  index: -1
바리2얼굴
  rotate: false
  xy: 1426, 3604
  size: 160, 155
  orig: 162, 157
  offset: 1, 1
  index: -1
바리2옆머리
  rotate: true
  xy: 3893, 3808
  size: 127, 151
  orig: 127, 156
  offset: 0, 5
  index: -1
배게
  rotate: true
  xy: 706, 2749
  size: 340, 350
  orig: 342, 467
  offset: 1, 1
  index: -1
사이드머리
  rotate: true
  xy: 2923, 4047
  size: 47, 212
  orig: 47, 212
  offset: 0, 0
  index: -1
상체
  rotate: true
  xy: 1089, 3375
  size: 227, 387
  orig: 227, 387
  offset: 0, 0
  index: -1
앞머리
  rotate: true
  xy: 2, 3924
  size: 87, 157
  orig: 87, 157
  offset: 0, 0
  index: -1
얼굴
  rotate: false
  xy: 1588, 3563
  size: 165, 155
  orig: 167, 157
  offset: 1, 1
  index: -1
옆머리
  rotate: true
  xy: 469, 3911
  size: 97, 152
  orig: 97, 152
  offset: 0, 0
  index: -1
오른손
  rotate: true
  xy: 1345, 3987
  size: 70, 91
  orig: 72, 93
  offset: 1, 1
  index: -1
오른팔
  rotate: true
  xy: 3159, 3502
  size: 182, 277
  orig: 182, 277
  offset: 0, 0
  index: -1
왼사이드머리
  rotate: true
  xy: 3509, 3672
  size: 147, 172
  orig: 147, 172
  offset: 0, 0
  index: -1
왼손
  rotate: false
  xy: 2, 4022
  size: 75, 60
  orig: 77, 62
  offset: 1, 1
  index: -1
왼팔
  rotate: true
  xy: 1787, 3303
  size: 233, 307
  orig: 237, 307
  offset: 0, 0
  index: -1
의자
  rotate: true
  xy: 2292, 3710
  size: 141, 300
  orig: 143, 302
  offset: 1, 1
  index: -1
이불
  rotate: false
  xy: 894, 2275
  size: 890, 425
  orig: 892, 912
  offset: 1, 1
  index: -1
친구
  rotate: true
  xy: 2096, 3288
  size: 235, 920
  orig: 237, 922
  offset: 1, 1
  index: -1
친구After
  rotate: true
  xy: 2, 3138
  size: 235, 920
  orig: 237, 922
  offset: 1, 1
  index: -1
친구Dinner
  rotate: true
  xy: 1226, 3051
  size: 235, 920
  orig: 237, 922
  offset: 1, 1
  index: -1
침대파츠
  rotate: false
  xy: 2, 1838
  size: 1080, 435
  orig: 1082, 437
  offset: 1, 1
  index: -1
캐릭터
  rotate: false
  xy: 1755, 3548
  size: 280, 170
  orig: 282, 172
  offset: 1, 1
  index: -1
캐릭터After
  rotate: false
  xy: 2037, 3538
  size: 280, 170
  orig: 282, 172
  offset: 1, 1
  index: -1
캐릭터Dinner
  rotate: false
  xy: 2319, 3537
  size: 280, 170
  orig: 282, 172
  offset: 1, 1
  index: -1
탁자
  rotate: false
  xy: 2086, 2655
  size: 512, 347
  orig: 514, 349
  offset: 1, 1
  index: -1
할배
  rotate: true
  xy: 1084, 1646
  size: 450, 1080
  orig: 452, 1082
  offset: 1, 1
  index: -1
할배 그림자
  rotate: true
  xy: 2, 1306
  size: 530, 1040
  orig: 532, 1042
  offset: 1, 1
  index: -1
홍조
  rotate: false
  xy: 860, 4064
  size: 40, 30
  orig: 42, 32
  offset: 1, 1
  index: -1

Character2.png
size: 4096,4096
format: RGBA8888
filter: Linear,Linear
repeat: none
After배경
  rotate: true
  xy: 2, 2864
  size: 1080, 1970
  orig: 1082, 1972
  offset: 1, 1
  index: -1
Dinner배경
  rotate: true
  xy: 1974, 2864
  size: 1080, 1970
  orig: 1082, 1972
  offset: 1, 1
  index: -1
Dinner빈병실
  rotate: true
  xy: 2, 1782
  size: 1080, 1970
  orig: 1082, 1972
  offset: 1, 1
  index: -1
WallDraw
  rotate: true
  xy: 1974, 1782
  size: 1080, 1920
  orig: 1082, 1922
  offset: 1, 1
  index: -1
WallDrawBlack
  rotate: true
  xy: 2, 700
  size: 1080, 1920
  orig: 1082, 1922
  offset: 1, 1
  index: -1
WallDrawWhite
  rotate: true
  xy: 1924, 700
  size: 1080, 1920
  orig: 1082, 1922
  offset: 1, 1
  index: -1

Character3.png
size: 4096,4096
format: RGBA8888
filter: Linear,Linear
repeat: none
2
  rotate: false
  xy: 1924, 1699
  size: 1920, 2195
  orig: 1922, 2197
  offset: 1, 1
  index: -1
3
  rotate: false
  xy: 2, 1649
  size: 1920, 2245
  orig: 1922, 2247
  offset: 1, 1
  index: -1
배경
  rotate: true
  xy: 2, 567
  size: 1080, 1970
  orig: 1082, 1972
  offset: 1, 1
  index: -1
보정(추가)
  rotate: true
  xy: 1974, 617
  size: 1080, 1920
  orig: 1082, 1922
  offset: 1, 1
  index: -1

Character4.png
size: 4096,4096
format: RGBA8888
filter: Linear,Linear
repeat: none
1
  rotate: false
  xy: 2, 1639
  size: 1895, 2180
  orig: 1897, 2182
  offset: 1, 1
  index: -1
할아버지 주인공친구
  rotate: true
  xy: 1899, 2739
  size: 1080, 1920
  orig: 1080, 1920
  offset: 0, 0
  index: -1
효과
  rotate: true
  xy: 1899, 1657
  size: 1080, 1920
  orig: 1082, 1922
  offset: 1, 1
  index: -1
